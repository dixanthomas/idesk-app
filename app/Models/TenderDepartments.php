<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenderDepartments extends Model
{
    use HasFactory;

    public function tenders()
    {
        return $this->belongsToMany(Tender::Class, 'tender_departments');
    }
}
