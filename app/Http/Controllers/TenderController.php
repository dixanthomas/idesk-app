<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Tender;
use Illuminate\Http\Request;

class TenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tenders =  Tender::paginate(15);

        $data = [
            'page_title' => 'Manage Tenders'
        ];



        return view('dashboard.services.index',compact('tenders'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $districts = Category::where('type','district')->pluck('title','id');
        $departments = Category::where('type','departments')->pluck('title','id');

        $data = [
            'tender' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/services',
            'page_title' => 'Add a New Tender'
        ];

        return view('dashboard.services.edit',compact('districts','departments'),$data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $lastDate = $request->get('closing_date').' '.date("H:i:s", strtotime($request->get('closing_time')));
        try {


            $tender = new Tender();
            $tender->tender_no       = $request->get('tender_no');
            $tender->locale          = 'ml';
            $tender->description     = $request->get('description');
            $tender->value           = $request->get('value');
            $tender->emd             = $request->get('emd');
            $tender->document_fee    = $request->get('document_fee');
            $tender->last_date       = $lastDate;
            $tender->sticky          = '0';
            $tender->status          = $request->get('status') ?? 0;
            $tender->save();


            $tender->departments()->sync($request->input('department'));
            $tender->districts()->sync($request->input('district'));

            return redirect('dashboard/services/'.$tender->id.'/edit')->with('success', 'Category Created Successfully!');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tender  $tender
     * @return \Illuminate\Http\Response
     */
    public function show(Tender $tender)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tender  $tender
     * @return \Illuminate\Http\Response
     */
    public function edit(Tender $tender)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tender  $tender
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tender $tender)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tender  $tender
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tender $tender)
    {
        //
    }
}
