<?php

namespace App\Http\Controllers;

use App\Models\TenderDepartments;
use Illuminate\Http\Request;

class TenderDepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TenderDepartments  $tenderDepartments
     * @return \Illuminate\Http\Response
     */
    public function show(TenderDepartments $tenderDepartments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TenderDepartments  $tenderDepartments
     * @return \Illuminate\Http\Response
     */
    public function edit(TenderDepartments $tenderDepartments)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TenderDepartments  $tenderDepartments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TenderDepartments $tenderDepartments)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TenderDepartments  $tenderDepartments
     * @return \Illuminate\Http\Response
     */
    public function destroy(TenderDepartments $tenderDepartments)
    {
        //
    }
}
