@extends('layouts.dashboard')

@section('content')
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">

                </div>
            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">

        <div class="row g-gs">
            <div class="col-lg-9 ">
                <div class="card card-bordered h-100">
                    <div class="card-inner">

                        {!! Form::model($tender, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}


                        <div class="form-group">
                            <label class="form-label" > Tender ID. <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('tender_no',null, ['class' => 'form-control ', 'placeholder'=>'Tender ID','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Description <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::textarea('description',null, ['class' => 'form-control','rows' => 1,'placeholder'=>'Enter Tender Description.','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Tender Value <span>*</span></label>
                            <div class="form-control-wrap">
                                <div class="form-text-hint"><span class="overline-title">INR</span></div>
                                {!! Form::number('value',null, ['class' => 'form-control','rows' => 1,'placeholder'=>'Enter Tender Value.','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class=" row gy-4">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Tender Closing Date <span>*</span></label>
                                    <div class="form-control-wrap">
                                        <div class="form-icon form-icon-left">
                                            <em class="icon ni ni-calendar"></em>
                                        </div>
                                        {!! Form::text('closing_date',null, ['class' => 'form-control date-picker ', 'placeholder'=>'Tender Closing Date','required' =>'required',' data-date-format'=>'yyyy-mm-dd']) !!}

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Closing Time <span>*</span></label>
                                    <div class="form-control-wrap">
                                        <div class="form-icon form-icon-left">
                                            <em class="icon ni ni-clock"></em>
                                        </div>
                                        {!! Form::text('closing_time',null, ['class' => 'form-control  time-picker ', 'placeholder'=>'Tender Closing Time','required' =>'required',' data-date-format'=>'yyyy-mm-dd']) !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" row gy-4">
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <label class="form-label">Earnest Money Deposit </label>

                                    <div class="form-control-wrap">
                                        <div class="form-text-hint"><span class="overline-title">INR</span></div>
                                        {!! Form::number('emd',null, ['class' => 'form-control','rows' => 1,'placeholder'=>'Enter Earnest Money Deposit ']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" for="phone-no">Document Fee </label>
                                    <div class="form-control-wrap">
                                        <div class="form-text-hint"><span class="overline-title">INR</span></div>
                                        {!! Form::number('document_fee',null, ['class' => 'form-control','rows' => 1,'placeholder'=>'Enter Document Fee']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-4">
                            <label class="form-label" >Select  Department<span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('department', $departments ,null, ['data-parsley-errors-container' => '#select-department','data-placeholder' => 'Select Department','class' => 'form-control form-select', 'data-search'=>'on', 'required' =>'required']) !!}
                            </div>
                            <div id="select-department"></div>
                        </div>

                        <div class="form-group mt-4">
                            <label class="form-label" >Select District<span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('district', $districts ,null, ['data-parsley-errors-container' => '#select-district','data-placeholder' => 'Select District','class' => 'form-control form-select', 'data-search'=>'on', 'required' =>'required']) !!}
                            </div>
                            <div id="select-district"></div>
                        </div>


                        <div class="form-group">
                            <label class="form-label" > Status <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('status',[''=>'','0'=>'Active','1'=>'Inactive'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select  Status','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>

                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-lg btn-outline-secondary">Reset</button>
                            <button type="submit" class="btn btn-lg btn-primary">Save</button>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
        </div>
    </div><!-- .nk-block -->


@endsection
